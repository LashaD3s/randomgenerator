package com.example.homework1

import android.os.Bundle
import android.util.Log.d
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.layout_generator.*

class ActivityGenerator : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_generator)
        init()
    }


    private fun init() {
        button1.setOnClickListener {
            d("button1", "button is active");
            button()
        }
    }

    private fun button(){
        val number:Int = (-100..100).random()
        d("button", "$number")
        if (number%5 == 0){
            if (number/5 > 0) {
                generatedtext.text = "Yes"
        }else{
                generatedtext.text = "No"
            }
            }

    }


}